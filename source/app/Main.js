/**
 * The Main JS File.
 * @module main
 * @param {string} author - The author of the book.
 */
function Main (){
	// private methods and properties
	console.log( "Hello World!" );
	// public methods and properties
	return {

	}	
}

module.exports = Main();