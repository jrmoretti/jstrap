/**
 * Dev task. 
 * This is default task to be used for development.
 */
var gulp = require( 'gulp' );
var settings = require( '../../package.json').settings;

gulp.task( 'dev' , ['app', 'static', 'styles', 'templates', 'vendor', 'browsersync'], function(){
	gulp.watch(settings.folders.app + '**', ['app']); // compile app using browserify
	gulp.watch(settings.folders.static + '**', ['static']); // prepare static files
	gulp.watch(settings.folders.styles + '**', ['styles']); // compile styles using stylus
	gulp.watch(settings.folders.templates + '**', ['templates']); // compile templates using jade
	gulp.watch(settings.folders.vendor + '**', ['vendor']); // compile libs
});