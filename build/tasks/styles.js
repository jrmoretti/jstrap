var gulp = require('gulp');
var stylus = require('gulp-stylus');
var nib = require('nib');
var minifyCSS = require('gulp-minify-css');
var settings = require( '../../package.json').settings;

gulp.task('styles', function(){
	gulp.src("./"+settings.folders.styles+"styles.styl")
		.pipe(stylus({use: nib(), compress: true}))
		.pipe(minifyCSS())
		.pipe(gulp.dest(settings.folders.public+"css/"));
});