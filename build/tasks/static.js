var gulp = require('gulp');
var settings = require( '../../package.json').settings;

gulp.task('static', ['html', 'images'], function() {
	gulp.src([ "./"+settings.folders.static+"/**/*.*", "!./"+settings.folders.static+"/img/*.*", "!./"+settings.folders.static+"/**/*.html"])
		.pipe(gulp.dest(settings.folders.public))
});