var jsdoc = require("gulp-jsdoc");
var gulp = require( 'gulp' );
var settings = require( '../../package.json').settings;

gulp.task('docs', function(){
	gulp.src(["./"+settings.folders.app+"/**/*.js", "README.md"])
	  .pipe(jsdoc(settings.folders.docs))
});