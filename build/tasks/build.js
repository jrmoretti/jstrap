/**
 * Build task.
 * Task used to prepare package for deployment.
 */
var gulp        = require('gulp');

gulp.task( 'build' , function(){
	console.log( "Build task started @ " + ( new Date() ) );
});