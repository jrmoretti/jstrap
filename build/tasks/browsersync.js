var gulp        = require('gulp');
var browserSync = require('browser-sync');
var settings = require( '../../package.json').settings;
// Static server
gulp.task('browsersync', function() {
	var options = { server: { baseDir: settings.folders.public } };
	
	if(settings.hostname && settings.hostname != "" && settings.hostname != "localhost")
		options.proxy = settings.hostname;

	if(settings.port && settings.port != 3000)
		options.port = settings.port;

	browserSync.init( [ settings.folders.public +'/**'], options );
});