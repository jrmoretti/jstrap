var browserify = require('browserify');
var gulp = require( 'gulp' );
var jshint = require('gulp-jshint');
var rename = require('gulp-rename');
var settings = require( '../../package.json').settings;
var source = require('vinyl-source-stream');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');

gulp.task('browserify', function(){

	browserify({
			debug:"true", 
			entries: ["./"+settings.folders.app+"Main.js"],
			paths: [settings.folders.app]
		})
			.bundle()
			.on('error', function(err) {
				console.log(err);
			})
			.pipe(source('Main.js'))
			.pipe(jshint())
			.pipe(jshint.reporter('default'))
			.pipe(rename('app.js'))
			.pipe(gulp.dest(settings.folders.public+"js/"));
});