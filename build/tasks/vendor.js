var gulp = require('gulp');
var concat = require('gulp-concat');
var settings = require( '../../package.json').settings;
var vendor_sources = require( '../../package.json').vendor;

gulp.task('vendor', function(){
	var source = [];
	for (var key in vendor_sources)
		source.push("./"+settings.folders.vendor + vendor_sources[key]);
	gulp.src(source)
		.pipe(concat('vendor.js'))
		.pipe(gulp.dest(settings.folders.public+'js/'));
});