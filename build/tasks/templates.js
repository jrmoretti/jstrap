var gulp = require('gulp');
var defineModule = require('gulp-define-module');
var jade = require('gulp-jade');
var settings = require( '../../package.json').settings;

gulp.task('templates', function(){
	gulp.src("./"+settings.folders.templates+'**/*.jade')
		.pipe(jade({ client: true }))
		.pipe(defineModule('plain', { wrapper: "module.exports = <%= contents %>"}))
		.pipe(gulp.dest(settings.folders.app+"templates/"))
});