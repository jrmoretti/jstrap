var gulp = require('gulp');
var htmlreplace = require('gulp-html-replace');
var settings = require( '../../package.json').settings;

gulp.task('html', function() {
	gulp.src("./"+settings.folders.static + 'index.html')
		.pipe(htmlreplace({
			'css': 'css/styles.css',
			'js': ['js/app.js', 'js/vendor.js'],
			'page_title' : settings.title
		}))
		.pipe(gulp.dest(settings.folders.public));
});