// get everything from the tasks folder
require( 'require-dir' )( './tasks' , { recurse: true } );

// set default task
var gulp = require( 'gulp' );

gulp.task( 'default' , [ 'dev' ] ); // default task is used on dev mode. Allows source maps and console outputs. Non-optimized images
gulp.task( 'deploy' , [ 'build' ] ); // prepare everything for deploy. Compresses everything. Remove console outputs. Optimizes images